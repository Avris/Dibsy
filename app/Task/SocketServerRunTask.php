<?php
namespace App\Task;

use Avris\Micrus\Console\Task;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SocketServerRunTask extends Task
{
    public function configure()
    {
        $this
            ->setName('socket:server:run')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container->get('socketServer')->configure($output)->run();
    }
}
