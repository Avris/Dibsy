<?php
namespace App\Task;

use App\Model\Authenticator;
use App\Model\Project;
use App\Model\ProjectUser;
use App\Model\Server;
use App\Model\User;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Doctrine\DoctrineFixturesTask;
use Avris\Micrus\Tool\FileDownloader;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesTask extends DoctrineFixturesTask
{
    protected static $servers = ['qa01', 'qa02', 'qa03', 'qa04', 'qa05', 'staging'];

    protected function load(OutputInterface $output)
    {
        $this->truncateDatabase();

        /** @var CryptInterface $crypt */
        $crypt = $this->container->get('crypt');
        $dir = $this->container->get('picDir');

        $admin = new User('prusinowski.andrzej@gmail.com');
        $admin->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('admin'));
        $admin->setRole(User::ROLE_ADMIN);
        $admin->setAvatar($this->generateAvatar('a')->moveToRandom($dir));
        $this->em->persist($admin);

        $user1 = new User('labeo77@gmail.com');
        $user1->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('user'));
        $user1->setAvatar($this->generateAvatar('u')->moveToRandom($dir));
        $this->em->persist($user1);

        $user2 = new User('labeo77+2@gmail.com');
        $user2->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('user'));
        $user2->setAvatar($this->generateAvatar('2')->moveToRandom($dir));
        $this->em->persist($user2);

        $user3 = new User('labeo77+3@gmail.com');
        $user3->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('user'));
        $user3->setAvatar($this->generateAvatar('3')->moveToRandom($dir));
        $this->em->persist($user3);

        $testProject = $this->generateProject('Test', [
            [$admin, true],
            [$user1, false],
            [$user2, false],
            [$user3, false],
        ]);
        $apiKey = new Authenticator();
        $testProject->addApiKeys($apiKey);
        $apiKey->set('name', 'Test');
        $apiKey->set('token', 'ae32e6bdbf547b107934b7ae3b3659f0fc8d492aaba8f7d0dd0d682a01d9837a');
        $this->em->persist($testProject);

        $this->generateProject('Finvera', [
            [$admin, true],
        ]);

        foreach (['Lorem', 'Ipsum', 'Foo', 'Bar', 'Rocket'] as $name) {
            $this->generateProject($name, [
                [$admin, true],
            ], false);
        }

        $this->em->flush();
    }

    protected function generateProject($name, array $users, $logo = true)
    {
        $project = new Project($name);
        $project->setUrlTemplate(sprintf('https://www-{{server}}.%s.de', strtolower($name)));
        $project->setAttachment($logo ? $this->downloadAvatar() : $this->generateAvatar($name));
        $project->handleFileUpload($this->container->get('picDir'));
        $this->em->persist($project);

        foreach ($users as list($user, $canManage)) {
            $this->em->persist(new ProjectUser($project, $user, $canManage, true));
        }

        foreach (static::$servers as $name) {
            $this->em->persist(new Server($project, $name));
        }

        return $project;
    }

    /**
     * @return UploadedFile
     */
    protected function downloadAvatar()
    {
        return (new FileDownloader())->download('http://lorempizza.com/240/240/'.uniqid().'.jpg');
    }

    /**
     * @return UploadedFile
     */
    protected function generateAvatar($name)
    {
        return $this->container->get('avatarGenerator')->generate($name);
    }
}
