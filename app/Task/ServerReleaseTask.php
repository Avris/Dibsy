<?php
namespace App\Task;

use App\Model\Repository\ServerUpdateRepository;
use App\Model\ServerUpdate;
use App\Event\UpdateServerEvent;
use Avris\Micrus\Console\Task;
use Avris\Micrus\Social\MailManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;

class ServerReleaseTask extends Task
{
    public function configure()
    {
        $this
            ->setName('server:release')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lock = new LockHandler('server:release');
        if (!$lock->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        /** @var MailManager $mailManager */
        $mailManager = $this->container->get('mailManager');

        /** @var ServerUpdateRepository $repo */
        $repo = $this->em->getRepository('ServerUpdate');

        foreach ($repo->findSheduledForAutoRelease() as $update) {
            $this->em->persist($update->getServer()->release());
            $this->dispatcher->trigger(new UpdateServerEvent($update->getServer()));
            $mailManager->send('autoRelease', $update->getUser(), $this->buildMailVars($update), true);
            $output->writeln('Automatic release: ' . $update->getId());
        }
        $this->em->flush();

        foreach ($repo->findSheduledForInactivityRelease() as $update) {
            $this->em->persist($update->getServer()->release());
            $this->dispatcher->trigger(new UpdateServerEvent($update->getServer()));
            $mailManager->send('inactivityRelease', $update->getUser(), $this->buildMailVars($update), true);
            $output->writeln('Inactivity release: ' . $update->getId());
        }
        $this->em->flush();

        foreach ($repo->findSheduledForReminder() as $update) {
            $this->em->persist($update->reminderSent());
            $mailManager->send('releaseReminder', $update->getUser(), $this->buildMailVars($update), true);
            $output->writeln('Release reminder: ' . $update->getId());
        }
        $this->em->flush();
    }

    protected function buildMailVars(ServerUpdate $update)
    {
        $projectName = $update->getServer()->getProject()->getName();

        return [
            'server' => $update->getServer()->getName(),
            'project' => $projectName,
            'projectLink' => $this->router->getUrl('projectShow', ['name' => $projectName], true)
        ];
    }
}
