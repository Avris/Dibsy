<?php
namespace App\Form\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\FormObject;
use Avris\Micrus\Forms\Widget\Widget;

class ForeignId extends Widget
{
    protected function getTemplate($widgetValue = null)
    {
        $template = $this->options->get('template');

        $object = $this->getValue();

        if (is_callable($template)) {
            return $template($object);
        }

        /** @var callable $href */
        $href = $this->options->get('href');

        return sprintf(
            '<span><input id="{id}" name="{name}" type="hidden" value="%s"/><a href="%s" target="_blank">%s</a></span>',
            FormObject::get($object, 'id'),
            $href($object),
            (string) $object
        );
    }

    public function valueFormToObject($value)
    {
        $model = $this->options->get('model');

        return $value ? $this->fetchService('orm')->findOneBy($model, 'id', $value) : null;
    }

    public function valueObjectToForm($value)
    {
        return $value ? FormObject::get($value, 'id') : null;
    }
}
