<?php
namespace App\Form\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget\Widget;

class Token extends Widget
{
    protected function getTemplate($widgetValue = null)
    {
        if (!$widgetValue) {
            return '';
        }

        return '<div class="input-group">
            <input id="{id}" name="{name}" type="text" value="{value}"
                class="{widget_class}" readonly {asserts} {attributes} {extra}/>
            <div class="input-group-addon btn-clipboard" data-clipboard-target="#{id}">
                <span class="fa fa-clipboard"></span>
            </div>
        </div>';
    }

    public function isReadonly()
    {
        return true;
    }
}
