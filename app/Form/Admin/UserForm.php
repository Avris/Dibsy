<?php
namespace App\Form\Admin;

use App\Model\User;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class UserForm extends Form
{
    public function configure()
    {
        $this
            ->add('email', Widget\Email::class, [], new Assert\NotBlank())
            ->add('role', Widget\Choice::class, [
                'choices' => User::$availableRoles,
            ], [
                new Assert\NotBlank(),
            ])
        ;
    }
}
