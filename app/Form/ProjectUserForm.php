<?php
namespace App\Form;

use App\Form\Widget\ForeignId;
use App\Model\User;
use Avris\Micrus\Controller\Routing\Service\Router;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class ProjectUserForm extends Form
{
    public function configure()
    {
        $loggedInUser = $this->container->get('securityManager.user');
        $formUser = $this->getObject()->getUser();
        $editingSelf = $loggedInUser === $formUser;

        $this
            ->add('user', ForeignId::class, [
                'model' => User::class,
                'href' => function (User $user) {
                    return 'mailto:' . $user->getEmail();
                },
                'readonly' => true,
            ], [])
            ->add('createdAt', Widget\Display::class, [
                'template' => function (\DateTime $value) {
                    return $value->format(l('format.dateTime'));
                }
            ])
            ->add('canManage', Widget\Checkbox::class,
                $editingSelf
                    ? ['readonly' => true, 'attr' => ['disabled' => 'disabled']]
                    : []
            )
        ;
    }
}
