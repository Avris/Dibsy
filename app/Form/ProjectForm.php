<?php
namespace App\Form;

use App\Form\Assert\UrlTemplate;
use App\Model\Authenticator;
use App\Model\Project;
use App\Model\ProjectUser;
use App\Model\Server;
use App\Service\Github\GithubService;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class ProjectForm extends Form
{
    public function configure()
    {
        $router = $this->container->get('router');

        $isNew = !$this->getObject(false)->id;
        $loggedInUser = $this->container->get('securityManager.user');

        $this
            ->add('name', Widget\Text::class, [], [
                new Assert\NotBlank(),
                new Assert\Regexp('^\w[\w-.]*\w$', l('validator.NameFormat')),
                new Assert\Unique(
                    $this->container->get('orm'),
                    $this->object,
                    'Project',
                    'name',
                    l('entity.Project.fields.nameTaken')
                ),
            ])
            ->add('attachment', Widget\File::class, [], [
                new Assert\File\File(),
                new Assert\File\MaxSize('5M'),
                new Assert\File\Image(),
                new Assert\File\Extension(['jpg', 'jpeg', 'png']),
                new Assert\File\MaxWidth(2000),
                new Assert\File\MaxWidth(2000),
                new Assert\File\MinWidth(240),
                new Assert\File\MinWidth(240),
            ], !$isNew)
            ->add('logo', Widget\Display::class, [
                'label' => '',
                'template' => function($value) use ($router) {
                    return $value
                        ? '<img src="'.$router->getUrl('imagineGenerate', [
                            'generator' => 'thumb',
                            'filename' => $value,
                        ], '').'" class="img-sm"/>'
                        : false;
                },
            ], [], !$isNew)
            ->add('urlTemplate', Widget\Text::class, [
                'placeholder' => 'https://{{server}}.myproject.eu',
            ], [
                new UrlTemplate,
            ])
            ->add('githubRepo', Widget\Text::class, [], [
                new Assert\Regexp(GithubService::REPO_REGEX),
            ])
            ->add('servers', Widget\MultipleSubForm::class, [
                'form' => ServerForm::class,
                'model' => Server::class,
                'container' => $this->container,
                'add' => true,
                'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                'remove' => true,
                'btnRemoveText' => '<span class="fa fa-trash"></span>',
            ], [
                new Assert\UniqueField('name'),
                new Assert\NotBlank(),
                new Assert\MinCount(1),
                new Assert\MaxCount(20),
            ])
            ->add('projectUsers', Widget\MultipleSubForm::class, [
                'form' => ProjectUserForm::class,
                'model' => ProjectUser::class,
                'container' => $this->container,
                'remove' => function (ProjectUser $projectUser) use ($loggedInUser) {
                    return $projectUser->getUser() != $loggedInUser;
                },
                'btnRemoveText' => '<span class="fa fa-trash"></span>',
                'btnRemoveAttr' => sprintf('data-confirm="%s"', htmlentities(l('entity.Project.revoke.confirm'))),
             ], [
                new Assert\NotBlank,
                new Assert\UniqueField('user'),
                new Assert\ObjectValidator(function (Project $project) {
                    return $project->hasManager($this->container->get('securityManager.user'))
                        ? true
                        : l('entity.Project.revoke.cannotRevokeYourself');
                }, $this->getObject())
            ], !$isNew)
            ->add('apiKeys', Widget\MultipleSubForm::class, [
                'form' => ApiKeyForm::class,
                'model' => Authenticator::class,
                'container' => $this->container,
                'add' => true,
                'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                'remove' => true,
                'btnRemoveText' => '<span class="fa fa-trash"></span>',
            ], [
                new Assert\UniqueField('name'),
            ])
            ->add('webhooks', Widget\MultipleSubForm::class, [
                'form' => WebhookForm::class,
                'model' => Authenticator::class,
                'container' => $this->container,
                'add' => true,
                'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                'remove' => true,
                'btnRemoveText' => '<span class="fa fa-trash"></span>',
            ], [
                new Assert\UniqueField('url'),
            ])
        ;
    }
}
