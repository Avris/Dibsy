<?php
namespace App\Form;

use App\Form\Assert\NewProjectUser;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class InviteForm extends Form
{
    public function configure()
    {
        $this
            ->add('emails', Widget\MultipleWidget::class, [
                'widget' => Widget\Email::class,
                'widgetOptions' => [
                    'placeholder' => 'user@domain.eu',
                ],
                'widgetAsserts' => [
                    new Assert\NotBlank,
                    new NewProjectUser($this->getOptions('project')),
                ],
                'add' => true,
                'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                'remove' => true,
                'btnRemoveText' => '<span class="fa fa-trash"></span>',
            ], [
                new Assert\NotBlank(),
                new Assert\MinCount(1),
                new Assert\MaxCount(10),
            ])
        ;
    }
}
