<?php
namespace App\Form;

use App\Form\Widget\Token;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class ApiKeyForm extends Form
{
    public function configure()
    {
        $this
            ->add('name', Widget\Text::class, [
                l('api.keys.name')
            ], [
                new Assert\NotBlank()
            ])
            ->add('token', Token::class, [
                l('api.keys.token')
            ])
        ;
    }
}
