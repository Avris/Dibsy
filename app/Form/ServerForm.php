<?php
namespace App\Form;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Widget\Email;
use Avris\Micrus\Forms\Widget\Text;
use Avris\Micrus\Forms\Widget\Url;

class ServerForm extends Form
{
    public function configure()
    {
        $this
            ->add('name', Text::class, [], [
                new Assert\NotBlank(),
                new Assert\Regexp('^\w[\w-.]*\w$', l('validator.NameFormat'))
            ])
            ->add('url', Url::class, [], [])
        ;
    }
}
