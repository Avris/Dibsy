<?php
namespace App\Form;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class TakeForm extends Form
{
    public function configure()
    {
        $this
            ->add('branch', Widget\TextAddon::class, [
                'after' => '<span class="fa fa-spinner fa-pulse fa-fw"></span>'
            ])
            ->add('autoRelease', Widget\DateTime::class, [
                'text' => true,
                'class' => 'datetimepicker',
                'attr' => [
                    'data-min-date' => (new \DateTime('now'))->format('Y-m-d H:i'),
                    'data-max-date' => (new \DateTime('+7 days 1 minute'))->format('Y-m-d H:i'),
                ]
            ], [
                new Assert\MinDate('now', l('entity.ServerUpdate.autoRelease.error.MinDate')),
                new Assert\MaxDate('+7 days', l('entity.ServerUpdate.autoRelease.error.MaxDate')),
            ])
        ;
    }
}
