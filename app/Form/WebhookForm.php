<?php
namespace App\Form;

use App\Form\Widget\Token;
use App\Model\Authenticator;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class WebhookForm extends Form
{
    public function configure()
    {
        $this
            ->add('url', Widget\Url::class, [
                'label' => l('entity.Project.webhooks.fields.url'),
            ], [
                new Assert\NotBlank()
            ])
            ->add('events', Widget\Choice::class, [
                'label' => l('entity.Project.webhooks.fields.events'),
                'choices' => array_combine(Authenticator::getWebhookEvents(), Authenticator::getWebhookEvents()),
                'choiceTranslation' => 'entity.Project.webhooks.events.',
                'multiple' => true,
                'expanded' => true,
            ], [
                new Assert\NotBlank(),
            ])
            ->add('token', Token::class, [
                'label' => l('entity.Project.webhooks.fields.token'),
            ])
        ;
    }
}
