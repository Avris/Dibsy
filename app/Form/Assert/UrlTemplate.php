<?php
namespace App\Form\Assert;

use App\Model\Project;
use App\Model\User;
use Avris\Micrus\Forms\Assert\Assert;

class UrlTemplate extends Assert
{
    /**
     * @param $value
     * @return true|string (returns true when valid, string with error message otherwise)
     */
    public function validate($value)
    {
        if (mb_strpos($value, '{{server}}') === false) {
            return $this->message;
        }

        if (!filter_var(str_replace('{{server}}', 'server', $value), FILTER_VALIDATE_URL)) {
            return $this->message;
        }

        return true;
    }
}
