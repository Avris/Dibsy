<?php
namespace App\Form\Assert;

use App\Model\Project;
use App\Model\User;
use Avris\Micrus\Forms\Assert\Assert;

class NewProjectUser extends Assert
{
    public function __construct(Project $project, $message = false)
    {
        $this->project = $project;
        parent::__construct($message);
    }

    /**
     * @param $value
     * @return true|string (returns true when valid, string with error message otherwise)
     */
    public function validate($value)
    {
        $alreadyHasAccess = $this->project->getUsers()->exists(function ($i, User $user) use ($value) {
            return $user->getEmail() === strtolower(trim($value));
        });

        return $alreadyHasAccess ? $this->message : true;
    }
}
