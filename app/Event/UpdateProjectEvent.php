<?php
namespace App\Event;

use App\Model\Project;
use Avris\Micrus\Bootstrap\Event;

class UpdateProjectEvent extends Event
{
    /** @var Project */
    protected $project;

    /**
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'updateProject';
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
