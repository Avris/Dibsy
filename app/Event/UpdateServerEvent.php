<?php
namespace App\Event;

use App\Model\Server;
use Avris\Micrus\Bootstrap\Event;

class UpdateServerEvent extends Event
{
    /** @var Server */
    protected $server;

    /**
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'updateServer';
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }
}
