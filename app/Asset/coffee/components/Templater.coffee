class @Templater
  badge: (user) ->
    if user?
      """
<img src="#{M.route('imagineGenerate', {generator: 'avatar', filename: user.avatar}, '')}"
  class="avatar avatar-big"
  data-toggle="tooltip"
  data-placement="top"
  title="#{user.identifier}">
"""
    else
      """
<img src="#{M.asset('assetic/gfx/system.png')}"
  class="avatar avatar-big"
  data-toggle="tooltip"
  data-placement="top"
  title="#{M.l('entity.User.system')}">
"""


  timeDiff: (datetime) ->
    """
<span class="timediff avatar-margin" data-toggle="tooltip" data-placement="top" title="#{datetime}">
    #{(new TimeDiff).diff(datetime)}
</span>
"""


  serverStatus: (status) ->
    if status.taken
      @badge(status.user) + @timeDiff(status.createdAt)
    else """
<span class="label label-success">
    #{M.l('entity.ServerUpdate.release.free')}
</span>
"""

  short: (text) -> """<span class="short">#{if text then text else ''}</span>"""

  autoRelease: (status) ->
    return '' unless status.autoRelease
    return """
<div class="small text-center"
     data-toggle="tooltip"
     data-placement="top"
     title="#{ M.l('entity.ServerUpdate.fields.autoRelease') }">
    <span class="fa fa-clock-o"></span>
    #{ status.autoRelease }
</div>
"""

  serverAction: (status, projectName, serverName) ->
    if status.taken
      route = 'serverRelease'
      icon = 'fa fa-unlock'
      if not status.user? or M.user.id == status.user.id
        btnClass = 'btn-success'
        action = 'release.action'
      else if M.canManageProject
        btnClass = 'btn-danger'
        action = 'release.force'
      else
        return ""
    else
      route = 'serverTake'
      icon = 'fa fa-lock'
      btnClass = 'btn-default'
      action = 'take.action'

    return """
#{@autoRelease(status)}
<a href="#{M.route(route, {name: projectName, server_name: serverName})}" class="btn #{btnClass} btn-xs btn-block">
    <span class="#{icon}"></span>
      #{M.l('entity.ServerUpdate.'+action)}
</a>
"""
