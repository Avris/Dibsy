Array.cast = (el) -> if Array.isArray(el) then el else if el == undefined then [] else [el]
Array.intersection = (a, b) ->
  [a, b] = [b, a] if a.length > b.length
  value for value in a when value in b
