$.fn.alert.Constructor.TRANSITION_DURATION = 500
$container = $('.notifications')
template = $container.find('script').html()
originalTitle = document.title

#TODO
#$(window).focus ->
#  console.log 'focus'
#$(window).blur ->
#  console.log 'blur'

class @Notification
  constructor: (@type, @content, @timeout = 0) ->
    $notification = $(template.replace('%type%', @type).replace('%content%', @content))
    $notification.on 'close.bs.alert', -> Notification.refreshCount(-1)
    $container.append $notification
    setTimeout(->
      $notification.addClass 'in'
    , 100)
    Notification.refreshCount()

#    TODO
#    if @timeout
#      alert(@timeout)


  @refreshCount: (predictChange = 0) ->
    l = $container.find('.alert').length + predictChange
    document.title = if l then "(#{l}) #{originalTitle}" else originalTitle
