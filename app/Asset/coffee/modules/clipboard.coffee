showTooltip = (elem, msg) ->
  $(elem).addClass('tooltipped tooltipped-s').attr('aria-label', msg)

fallbackMessage = (action) ->
  actionKey = if action == 'cut' then 'X' else 'C'
  switch
    when /iPhone|iPad/i.test(navigator.userAgent) then 'No support :('
    when /Mac/i.test(navigator.userAgent) then 'Press ⌘-' + actionKey + ' to ' + action
    else 'Press Ctrl-' + actionKey + ' to ' + action

$('.btn-clipboard').on 'mouseleave', (e) ->
  $(this).removeClass('tooltipped tooltipped-s').removeAttr('aria-label')

clipboard = new Clipboard('.btn-clipboard')
clipboard.on 'success', (e) ->
  showTooltip(e.trigger, 'Copied!')
clipboard.on 'error', (e) ->
  showTooltip(e.trigger, fallbackMessage(e.action))
