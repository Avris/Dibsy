$('.datetimepicker').each (i, el) ->
  $t = $(this)
  $t.datetimepicker
    sideBySide: true
    format: M.l('format.dateTimeMoment')
    icons:
      time: 'fa fa-clock-o'
      date: 'fa fa-calendar'
      up: 'fa fa-arrow-up'
      down: 'fa fa-arrow-down'
      previous: 'fa fa-arrow-left'
      next: 'fa fa-arrow-right'
      today: 'fa fa-calendar-check-o'
      clear: 'fa fa-trash'
      close: 'fa fa-times'
    minDate: $t.data('minDate')
    maxDate: $t.data('maxDate')