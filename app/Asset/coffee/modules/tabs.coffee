$('.tab-pane.active.in :input:enabled:visible:first').focus()

$('a[data-toggle="tab"]').on 'shown.bs.tab', (e) ->
  target = e.target.attributes.href.value
  $(target+' :input:enabled:visible:first').focus()

$('a[data-toggle="tab-external"]').click ->
  href = $(this).attr('href')
  $('a[data-toggle="tab"][href="'+href+'"]').click()
  false
