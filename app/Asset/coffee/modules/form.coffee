$('body').on 'click', '.form-multiple-add', ->
  $form = $(this).parents('.form-multiple')
  newIndices = $form.find('[data-index^=new]').map((i, el) -> el.dataset['index'].substr(3)).get()
  newIndex = if newIndices.length then Math.max.apply(null, newIndices) + 1 else 0
  $template = $($form.find('.form-multiple-add-template').html().replace(/%i%/g, 'new' + newIndex))
  $(this).parents('tr').before($template)
  $template.find(':input:enabled:visible:first').focus()
$('body').on 'click', '.form-multiple-remove', ->
  return false unless confirm(M.l('crud.delete.multiple.confirm'))
  $(this).parents('tr').remove()
