return unless $('#servers').length

@socket = new WebSocket("#{M.socketHost}/#{encodeURIComponent(cookieBag.get('rememberme'))}/#{M.routeMatch.tags.name}")

templater = new Templater

@socket.onmessage = (event) ->
  message = JSON.parse(event.data)
  switch message.event
    when 'updateServer'
      status = message.server.currentStatus
      $('#' + message.server.id).each (i, el) ->
        $tr = $(el)
        $tr.toggleClass('danger', status.taken)
        $tr.find('td:eq(1)').html(templater.serverStatus(status))
        $tr.find('td:eq(2)').html(templater.short(status.branch))
        $tr.find('td:eq(4)').html(templater.serverAction(status, message.projectName, message.server.name))
      restartAddons()
      new Notification(
        if status.taken then 'danger' else 'success'
        M.l(
          "entity.ServerUpdate.#{if status.taken then 'take' else 'release'}.notification",
          {
            user: if status.user then status.user.identifier else M.l('entity.User.system')
            project: message.projectName, server: message.server.name
          }
        )
      )
    when 'updateProject'
      new Notification(
        'warning',
        M.l(
          "entity.Project.update.notification",
          {project: message.projectName}
        )
      )

@socket.onclose = (event) ->
  if event.code != 1000
    console.log "Websocket connection closed, code #{event.code}, reason: #{event.reason}"
$(window).on 'beforeunload', -> @socket.close()
