isOverflowed = (element) -> element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth

@restartAddons = ->
  $('[data-toggle="tooltip"]').tooltip()
  $('.short').each (i, el) ->
    if isOverflowed(el)
      $(el)
        .data('toggle', 'tooltip')
        .attr('title', el.textContent)
        .tooltip()
  $('.select2').select2(
    theme: 'bootstrap'
  )
@restartAddons()

$('.btn-modal').click -> $($(this).attr('href')).modal(); false
$('.modal-active').each -> $(this).modal()

$('[data-confirm]').click -> confirm($(this).data('confirm'))
