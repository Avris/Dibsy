timeDiff = new TimeDiff
setInterval(->
  $('.timediff').each (i, el) ->
    $el = $(el)
    time = $el.attr('title') or $el.data('original-title')
    $el.html(timeDiff.diff(time))
, 60 * 1000)
