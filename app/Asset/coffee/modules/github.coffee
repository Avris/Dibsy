$input = $('#ServerUpdate_branch')
return unless $input.length

$addon = $input.next()
$group = $input.parent()

addPopover = (icon, title, content) ->
  $addon
    .html("<span class='fa fa-fw #{icon}'></span>")
    .data('toggle', 'popover')
    .attr('title', "<strong><span class='fa fa-fw #{icon}'></span> #{title}</strong>")
    .data('content', content)
    .data('placement', 'bottom')
    .data('html', true)
    .addClass('pointer')
    .popover()

hideAddon = ->
  $addon.remove()
  $group.removeClass('input-group')

$.get(
  M.route('apiFetchBranches', {name: M.routeMatch.tags.name})
  (res) ->
    switch res.result
      when 'success'
        hideAddon()
        $input.select2(
          data: res.branches
          theme: 'bootstrap'
        )
      when 'hint'
        addPopover 'fa-lightbulb-o', res.resultTrans, res.text
      when 'warning'
        addPopover 'fa-exclamation-triangle', res.resultTrans, res.text
      when 'error'
        addPopover 'fa-times-circle', res.resultTrans, "<pre>#{res.text}</pre>"
      when 'empty'
        hideAddon()
      else
        hideAddon()
)

