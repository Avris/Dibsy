<?php
namespace App\Controller\Admin;

use App\Event\UpdateProjectEvent;
use App\Form\ProjectForm;
use App\Model\Project;
use App\Model\Server;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Micrus\Crud\Config\GeneralConfig;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\CrudController;
use Avris\Micrus\Forms\FormObject;

class ProjectController extends CrudController
{
    protected function configureList(ListConfig $config)
    {
        $config
            ->add('logo', false, false, false, 'Crud/Project/logo')
            ->add('name', true)
            ->add('githubRepo', false, true, true, 'Crud/Project/githubRepo')
            ->add('servers', false, false, false, 'Crud/Project/servers')
            ->add('createdAt')
            ->addAction('Crud/Project/link')
        ;
    }

    protected function configureExport(ExportConfig $config)
    {
        $config
            ->add('name')
            ->add('urlTemplate')
            ->add('githubRepo')
            ->add('servers')
            ->add('createdAt')
            ->add('userCount', function (FormObject $entity) {
                return $entity->users->count();
            })
        ;
    }

    protected function configureShow(ShowConfig $config)
    {
        $config
            ->add('logo', false, false, 'Crud/Project/logoBig')
            ->add('name')
            ->add('urlTemplate')
            ->add('githubRepo', false, false, $view = 'Crud/Project/githubRepo')
            ->add('servers', false, false, $view = 'Crud/Project/servers')
            ->add('users', false, false, $view = 'Crud/Project/users')
        ;
    }

    protected function configureGeneral(GeneralConfig $config)
    {
        $config->addAction('Crud/Project/linkBig');
    }

    protected function getForm($entity)
    {
        return new ProjectForm($entity, $this->container);
    }

    protected function create()
    {
        return (new Project(''))
            ->addServers(new Server())
            ->addUser($this->getUser(), true, true);
    }

    /**
     * @param Project $entity
     */
    protected function postCreate($entity)
    {
        $entity->setAttachment(
            $this->get('avatarGenerator')->generate($entity->getName(), 320)
        );
        $this->postUpdate($entity);
    }

    /**
     * @param Project $entity
     */
    protected function postUpdate($entity)
    {
        $entity->handleFileUpload($this->get('picDir'));
        $this->adapter->persist($entity);

        foreach ($entity->getRemovedUsers() as $removedUser) {
            $this->get('mailManager')->send('accessRevoked', $removedUser, [
                'project' => $entity->getName(),
                'manager' => $this->getUser(),
            ], true);
        }

        $this->trigger(new UpdateProjectEvent($entity));
    }
}
