<?php
namespace App\Controller\Admin;

use App\Form\Admin\UserForm;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\CrudController;
use Avris\Micrus\Tool\FlashBag;

class UserController extends CrudController
{
    protected function configureList(ListConfig $config)
    {
        $config
            ->add('avatar', false, false, false, 'Crud/User/avatar')
            ->add('email', true)
            ->add('role', false, true, true, 'Crud/User/role')
            ->add('contact', false, false, false, $view = 'Crud/User/contact')
            ->add('createdAt')
            ->setDefaultSort('createdAt|desc')
            ->setDefaultFilters(['email' => '!~'])
        ;
    }

    protected function configureShow(ShowConfig $config)
    {
        $config
            ->add('avatar', false, false, 'Crud/User/avatarBig')
            ->add('email')
            ->add('role', false, false, 'Crud/User/role')
            ->add('contact', false, false, 'Crud/User/contact')
            ->add('projects', false, false, 'Crud/User/projects')
        ;
    }

    public function viewVars()
    {
        return [
            'socialSources' => $this->get('socialManager')->getSources(),
        ];
    }

    protected function getForm($entity)
    {
        return new UserForm($entity);
    }
}
