<?php
namespace App\Controller;

use App\Form\ApiKeyForm;
use App\Model\Authenticator;
use App\Model\Project;
use App\Service\Github\BadResponseException;
use App\Service\Github\NoRepoException;
use App\Service\Github\NoTokenException;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Http\FileResponse;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\RoleChecker;

class ApiController extends Controller
{
    /**
     * @M\Route("GET /api/v1")
     */
    public function referenceAction()
    {
        return $this->render([
            'base' => $this->getRequest()->getFullUrl(),
        ]);
    }

    /**
     * @M\Route("GET /api/Dibsy.postman_collection")
     */
    public function referencePostmanAction()
    {
        return new FileResponse($this->getRootDir() . '/app/Asset/api/Dibsy.postman_collection', true);
    }

    /**
     * @M\Route("GET /project/{name}/branches")
     * @M\Secure(check="canSeeProject")
     */
    public function fetchBranchesAction(RoleChecker $roleChecker, Project $project)
    {
        try {
            $branches = $this->get('github')->fetchBranches($project);

            return $this->renderJson(['result' => 'success', 'branches' => $branches]);
        } catch (NoRepoException $e) {
            return $this->getUser()->canManageProject($roleChecker, $project)
                ? $this->renderJson([
                    'result' => 'hint',
                    'resultTrans' => l('github.hint'),
                    'text' => l('github.hintRepo', [
                        'link' => $this->generateUrl('projectEdit', ['name' => $project->getName()]),
                    ])])
                : $this->renderJson(['result' => 'empty']);
        } catch (NoTokenException $e) {
            return $this->renderJson([
                'result' => 'hint',
                'resultTrans' => l('github.hint'),
                'text' => l('github.hintToken', [
                    'link' => $this->generateUrl('userAccount'),
                    'repo' => $project->getGithubRepo(),
                ])]);
        } catch (BadResponseException $e) {
            return $this->renderJson([
                'result' => 'error',
                'resultTrans' => l('github.error'),
                'text' => $e->getMessage()
            ]);
        }
    }
}
