<?php
namespace App\Controller\Api\v1;

use App\Model\Authenticator;
use App\Model\Project;
use Avris\Bag\Bag;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Model\AuthenticatorRepository;

class BaseApiController extends Controller
{
    /** @var Project */
    protected $project;

    /**
     * @param ContainerInterface $container
     * @throws UnauthorisedException
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        if ($this->get('env') === 'dev') {
            $this->project = $this->getEm()->getRepository(Project::class)
                ->findOneBy(['name' => $this->getRequest()->getQuery('project') ?: 'Test']);
            return;
        }

        $request = $container->get('request');
        $token = (string) $request->getHeaders('X-Token');

        if (!$token) {
            throw new UnauthorisedException('Invalid token', $request, $token);
        }

        /** @var AuthenticatorRepository $authrepo */
        $authrepo = $this->getEm()->getRepository(Authenticator::class);

        $auth = $authrepo->findByToken(Authenticator::TYPE_API_KEY, $token);
        if (!$auth || !$auth->getProject()) {
            throw new UnauthorisedException('Invalid token', $request, $token);
        }

        $this->project = $auth->getProject();
    }
}
