<?php
namespace App\Controller\Api\v1;

use App\Model\Server;
use App\Model\ServerUpdate;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Exception\NotFoundException;
use ICanBoogie\DateTime;

/**
 * @M\Route("/api/v1/project")
 */
class ProjectController extends BaseApiController
{
    /**
     * @M\Route("GET /")
     */
    public function showAction()
    {
        return $this->renderJson(
            $this->project
        );
    }
}
