<?php
namespace App\Controller\Api\v1;

use App\Model\ProjectUser;
use App\Model\ServerUpdate;
use App\Event\UpdateServerEvent;
use Avris\Bag\BagHelper;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Exception\BadRequestException;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Exception\UnauthorisedException;
use ICanBoogie\DateTime;

/**
 * @M\Route("/api/v1/servers")
 */
class ServerController extends BaseApiController
{
    /**
     * @M\Route("GET /")
     */
    public function listAction()
    {
        return $this->renderJson(
            $this->project->getServers()->toArray()
        );
    }

    /**
     * @M\Route("GET /{name}")
     */
    public function singleAction($serverName)
    {
        return $this->renderJson($this->project->getServer($serverName));
    }

    /**
     * @M\Route("POST /{name}/take")
     */
    public function takeAction($serverName)
    {
        $server = $this->project->getServer($serverName);

        $projectUser = $this->getProjectUser();
        $user = $projectUser ? $projectUser->getUser() : null;

        if ($server->getCurrentStatus()->isTaken() && $server->getCurrentStatus()->getUser() !== $user) {
            throw new BadRequestException('Server is already taken');
        }

        $update = new ServerUpdate($server, $user);
        $update->setBranch((string) BagHelper::magicGetter($this->getRequest()->getJsonBody(), 'branch'));
        $update->setAutoRelease($this->buildAutoRelease(
            BagHelper::magicGetter($this->getRequest()->getJsonBody(), ('autoRelease')
            )));
        $server->take($update);

        $this->getEm()->persist($server);
        $this->getEm()->flush();

        $this->trigger(new UpdateServerEvent($server));

        return $this->renderJson($server);
    }

    /**
     * @param string|null $value
     * @return DateTime|null
     * @throws BadRequestException
     */
    protected function buildAutoRelease($value)
    {
        if (!$value) {
            return null;
        }

        $value = new DateTime($value);
        if ($value < new DateTime('+10 minutes')) {
            throw new BadRequestException(l('entity.ServerUpdate.autoRelease.error.MinDate'));
        }

        if ($value > new DateTime('+7 days')) {
            throw new BadRequestException(l('entity.ServerUpdate.autoRelease.error.MaxDate'));
        }

        return $value;
    }

    /**
     * @return ProjectUser|null
     * @throws NotFoundException
     */
    protected function getProjectUser()
    {
        $userIdentifier = BagHelper::magicGetter($this->getRequest()->getJsonBody(), 'user');

        return $userIdentifier
            ? $this->project->getProjectUserByIdentifier(strtolower(trim($userIdentifier)))
            : null;
    }

    /**
     * @M\Route("POST /{name}/release")
     */
    public function releaseAction($serverName)
    {
        $server = $this->project->getServer($serverName);

        if (!$server->getCurrentStatus()->isTaken()) {
            throw new BadRequestException('Server is not taken');
        }

        $projectUser = $this->getProjectUser();

        if ($projectUser) {
            $isForce = $server->getCurrentStatus()->getUser()
                && $server->getCurrentStatus()->getUser() !== $projectUser->getUser();

            if ($isForce && !$projectUser->getCanManage()) {
                throw new UnauthorisedException(sprintf(
                    'You cannot release a server, because it\'s currently taken by %s',
                    $server->getCurrentStatus()->getUser() ?: l('entity.User.system')
                ));
            }

            if ($isForce) {
                $this->get('mailManager')->send('forceRelease', $server->getCurrentStatus()->getUser(), [
                    'server' => $server->getName(),
                    'project' => $server->getProject()->getName(),
                    'projectLink' => $this->getRequest()->getAbsoluteBase()
                        . $this->generateUrl('projectShow', ['name' => $server->getProject()->getName()]),
                    'manager' => $this->getUser(),
                ], true);
            }
        }

        $server->release($projectUser ? $projectUser->getUser() : null);
        $this->getEm()->persist($server);
        $this->getEm()->flush();

        $this->trigger(new UpdateServerEvent($server));

        return $this->renderJson($server);
    }

    /**
     * @M\Route("/{name}/history", methods="GET")
     */
    public function historyAction($serverName)
    {
        $server = $this->project->getServer($serverName);

        return $this->renderJson(
            $server->getRecentHistory($this->getQuery('limit') ?: 25)->toArray()
        );
    }
}
