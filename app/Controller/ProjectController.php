<?php
namespace App\Controller;

use App\Form\InviteForm;
use App\Form\ProjectForm;
use App\Model\Authenticator;
use App\Model\Project;
use App\Model\Server;
use App\Model\User;
use App\Event\UpdateProjectEvent;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Tool\FlashBag;

/**
 * @M\Secure
 * @M\Route("/project")
 */
class ProjectController extends Controller
{
    const SESSION_INVITATIONS = 'invitations';

    /**
     * @M\Route("/projects", options={"absolute": true})
     */
    public function listAction()
    {
        return $this->render([
            'projects' => $this->getUser()->getActiveProjects(),
        ]);
    }

    /**
     * @M\Route("/projects-all", options={"absolute": true})
     */
    public function allAction()
    {
        return $this->render([
            'projects' => $this->getEm()->getRepository(Project::class)->findAll(),
        ]);
    }

    /**
     * @M\Route("/{name}")
     * @M\Secure(check="canSeeProject")
     */
    public function showAction(Project $project)
    {
        return $this->render([
            'project' => $project,
            'socialSources' => $this->get('socialManager')->getSources(),
        ]);
    }

    /**
     * @M\Route("/projects/new", name="projectNew", options={"absolute": true})
     * @M\Route("/{name}/edit", name="projectEdit")
     * @M\Secure(check="canManageProject")
     */
    public function formAction(Project $project = null)
    {
        if (!$project) {
            $project = new Project('');
            $project->addServers(new Server());
            $project->addUser($this->getUser(), true, true);
        }

        $form = new ProjectForm($project, $this->container);
        $form->bindRequest($this->getRequest());

        if ($form->isValid()) {
            if (!$project->getId()) {
                $project->setAttachment(
                    $this->get('avatarGenerator')->generate($project->getName(), 320)
                );
            }
            $project->handleFileUpload($this->get('picDir'));
            $this->getEm()->persist($project);
            $this->getEm()->flush();

            foreach ($project->getRemovedUsers() as $removedUser) {
                $this->get('mailManager')->send('accessRevoked', $removedUser, [
                    'project' => $project->getName(),
                    'manager' => $this->getUser(),
                ], true);
            }

            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.save.success'));
            $this->trigger(new UpdateProjectEvent($project));

            return $this->redirectToRoute('projectEdit', ['name' => $project->getName()]);
        }

        return $this->render([
            'form' => $form,
            'project' => $project,
        ]);
    }

    /**
     * @M\Route("/{name}/join")
     * @M\Secure(check="cannotSeeProject")
     */
    public function joinAction(Project $project)
    {
        if ($project->getProjectUser($this->getUser())) {
            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.join.requested'));

            return $this->redirectToRoute('projectList');
        }

        if ($this->getRequest()->isPost()) {
            $project->addUser($this->getUser(), false, false);
            $this->getEm()->persist($project);
            $this->getEm()->flush();

            $this->get('mailManager')->send('joinRequest', $project->getManagers(), [
                'project' => $project->getName(),
                'projectLink' => $this->getRequest()->getAbsoluteBase()
                    . $this->generateUrl('projectShow', ['name' => $project->getName()]),
                'requestingUser' => $this->getUser(),
            ], true);

            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.join.requested'));

            return $this->redirectToRoute('projectList');
        }

        return $this->render([
            'project' => $project,
        ]);
    }

    /**
     * @M\Route("POST /{name}/request/{user}")
     * @M\Secure(check="canManageProject")
     */
    public function requestAction(Project $project, User $user)
    {
        $projectUser = $project->getProjectUser($user);
        $dataBag = $this->getRequest()->getData();

        if (!$projectUser || $projectUser->isActive()) {
            throw new NotFoundException(sprintf(
                'No membership request to %s by %s',
                $project->getName(),
                $user->getIdentifier())
            );
        }

        if ($dataBag->has('accept')) {
            $projectUser->setActive(true);
            $this->getEm()->persist($projectUser);
            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.request.accept.success'));
            $this->get('mailManager')->send('requestAccepted', $projectUser->getUser(), [
                'project' => $project->getName(),
                'projectLink' => $this->getRequest()->getAbsoluteBase()
                    . $this->generateUrl('projectShow', ['name' => $project->getName()]),
            ], true);
        } else if ($dataBag->has('decline')) {
            $this->getEm()->remove($projectUser);
            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.request.decline.success'));
            $this->get('mailManager')->send('requestDeclined', $projectUser->getUser(), [
                'project' => $project->getName(),
            ], true);
        } else {
            throw new NotFoundException('Invalid action');
        }

        $this->getEm()->flush();
        $this->trigger(new UpdateProjectEvent($project));

        return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
    }

    /**
     * @M\Route("/{name}/invite")
     * @M\Secure(check="canManageProject")
     */
    public function inviteAction(Project $project)
    {
        $form = new InviteForm(null, null, ['project' => $project]);
        $form->bindRequest($this->getRequest());
        if ($form->isValid()) {
            $this->get('invitation')->invite($project, $this->getUser(), $form->getObject()->emails);

            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.invite.success'));

            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        return $this->render([
            'form' => $form,
            'project' => $project,
        ]);
    }

    /**
     * @M\Route("/{name}/accept-invitation/{token}")
     * @M\Secure(public=true)
     */
    public function acceptInvitationAction(Project $project, $token)
    {
        /** @var Authenticator $auth */
        $auth = $this->getEm()->getRepository('Authenticator')->findByToken(Authenticator::TYPE_INVITATION, $token);
        if (!$auth || $auth->getPayload()['project'] !== $project->getId()) {
            $this->addFlash(FlashBag::DANGER, l('entity.Project.invite.invalidToken'));
            return $this->redirectToRoute('home');
        }

        if (!($user = $this->getUser())) {
            $this->getRequest()->getSession()->appendToElement(self::SESSION_INVITATIONS, $auth->getId());
            $this->addFlash(
                FlashBag::SUCCESS,
                l('entity.Project.invite.createAccount', ['project' => $project->getName()])
            );
            return $this->redirectToRoute('home');
        }

        if (!$this->get('invitation')->accept($this->getUser(), $auth, $project)) {
            $this->addFlash(FlashBag::WARNING, l('entity.Project.invite.youAlreadyHaveAccess'));
            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        $this->addFlash(FlashBag::SUCCESS, l('entity.Project.invite.granted', ['project' => $project->getName()]));
        $this->trigger(new UpdateProjectEvent($project));

        return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
    }

    /**
     * @M\Route("/{name}/leave")
     * @M\Secure(check="canSeeProject")
     */
    public function leaveAction(Project $project)
    {
        $projectUser = $project->getProjectUser($this->getUser());
        if ($projectUser->getCanManage() && $project->getManagers()->count() === 1) {
            $this->addFlash(FlashBag::DANGER, l('entity.Project.leave.onlyManager'));
            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        if ($this->getRequest()->isPost()) {
            $project->removeProjectUsers($projectUser);
            $this->getEm()->persist($project);
            $this->getEm()->flush();

            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.leave.success', ['project' => $project->getName()]));
            $this->trigger(new UpdateProjectEvent($project));

            return $this->redirectToRoute('home');
        }

        return $this->render([
            'project' => $project,
        ]);
    }

    /**
     * @M\Route("/{name}/remove")
     * @M\Secure(check="canManageProject")
     */
    public function removeAction(Project $project)
    {
        if ($this->getRequest()->isPost()) {
            $this->getEm()->remove($project);
            $this->getEm()->flush();

            $this->addFlash(FlashBag::SUCCESS, l('entity.Project.remove.success', ['project' => $project->getName()]));
            $this->trigger(new UpdateProjectEvent($project));

            return $this->redirectToRoute('home');
        }

        return $this->render([
            'project' => $project,
        ]);
    }
}
