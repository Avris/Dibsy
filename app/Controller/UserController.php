<?php
namespace App\Controller;

use Avris\Micrus\Social\SocialController;
use Avris\Micrus\Annotations as M;

class UserController extends SocialController
{
    /**
     * @M\Route("/", name="home")
     * @M\Route("/", name="userLogin")
     */
    public function loginAction()
    {
        return $this->getUser()
            ? $this->redirectToRoute('projectList')
            : parent::loginAction();
    }
}
