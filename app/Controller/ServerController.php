<?php
namespace App\Controller;

use App\Form\TakeForm;
use App\Model\Project;
use App\Model\Server;
use App\Model\ServerUpdate;
use App\Event\UpdateServerEvent;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Bootstrap\Container;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\RoleChecker;

/**
 * @M\Secure
 */
class ServerController extends Controller
{
    /**
     * @M\Route("/project/{name}/server/{server_name}")
     * @M\Secure(check="canSeeProject")
     */
    public function showAction(Project $project, Server $server)
    {
        return $this->render([
            'server' => $server,
        ]);
    }

    /**
     * @M\Route("/project/{name}/server/{server_name}/take")
     * @M\Secure(check="canSeeProject")
     */
    public function takeAction(Project $project, Server $server)
    {
        if ($server->getCurrentStatus()->isTaken()) {
            $this->addFlash(FlashBag::DANGER, l('entity.ServerUpdate.take.already'));

            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        $update = new ServerUpdate($server, $this->getUser());
        $form = new TakeForm($update, $this->container);

        $form->bindRequest($this->getRequest());
        if ($form->isValid()) {
            $server->take($update);
            $this->getEm()->persist($server);
            $this->getEm()->flush();

            $this->addFlash(FlashBag::SUCCESS, l('entity.ServerUpdate.take.success'));
            $this->trigger(new UpdateServerEvent($server));

            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        return $this->render([
            'server' => $server,
            'form' => $form,
        ]);
    }

    /**
     * @M\Route("/project/{name}/server/{server_name}/release")
     * @M\Secure(check="canSeeProject")
     */
    public function releaseAction(RoleChecker $roleChecker, Project $project, Server $server)
    {
        if (!$server->getCurrentStatus()->isTaken()) {
            $this->addFlash(FlashBag::DANGER, l('entity.ServerUpdate.release.already'));

            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        $isForce = $server->getCurrentStatus()->getUser()
            && $server->getCurrentStatus()->getUser() !== $this->getUser();

        if ($isForce && !$this->getUser()->canManageProject($roleChecker, $project)) {
            throw new UnauthorisedException;
        }

        if ($this->getRequest()->isPost()) {
            if ($isForce) {
                $this->get('mailManager')->send('forceRelease', $server->getCurrentStatus()->getUser(), [
                    'server' => $server->getName(),
                    'project' => $project->getName(),
                    'projectLink' => $this->getRequest()->getAbsoluteBase()
                        . $this->generateUrl('projectShow', ['name' => $project->getName()]),
                    'manager' => $this->getUser(),
                ], true);
            }

            $server->release($this->getUser());
            $this->getEm()->persist($server);
            $this->getEm()->flush();

            $this->addFlash(FlashBag::SUCCESS, l('entity.ServerUpdate.release.success'));
            $this->trigger(new UpdateServerEvent($server));

            return $this->redirectToRoute('projectShow', ['name' => $project->getName()]);
        }

        return $this->render([
            'server' => $server,
            'isForce' => $isForce,
        ]);
    }
}
