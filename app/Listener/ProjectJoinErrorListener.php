<?php
namespace App\Listener;

use App\Model\Project;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Routing\Service\Router;
use Avris\Micrus\Exception\Handler\ErrorEvent;
use Avris\Micrus\Exception\UnauthorisedException;

class ProjectJoinErrorListener
{
    /** @var Router */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param ErrorEvent $event
     */
    public function onError(ErrorEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof UnauthorisedException
            && $exception->getRequest()
            && isset($exception->getPayload()['restriction'])
            && $exception->getPayload()['restriction'] === 'canSeeProject'
            && isset($exception->getPayload()['tags']['name'])
        ) {
            /** @var Project $project */
            $project = $exception->getPayload()['tags']['name'];
            $response = new RedirectResponse($this->router->getUrl('projectJoin', ['name' => $project->getName()]));
            $event->setResponse($response);
        }
    }
}
