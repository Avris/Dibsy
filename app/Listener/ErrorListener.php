<?php
namespace App\Listener;

use Avris\Micrus\Controller\Http\JsonResponse;
use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\Handler\ErrorEvent;

class ErrorListener
{
    /** @var string */
    protected $env;

    /** @var Request */
    protected $request;

    public function __construct($env, Request $request)
    {
        $this->env = $env;
        $this->request = $request;
    }

    /**
     * @param ErrorEvent $event
     */
    public function onError(ErrorEvent $event)
    {
        if ($this->env !== 'dev' && substr($this->request->getCleanUrl(), 0, 5) === '/api/') {
            $event->setResponse($this->buildResponse($event->getException()));
        }
    }

    protected function buildResponse(\Exception $e)
    {
        return $e->getCode() >= 400 && $e->getCode() <= 499
            ? new JsonResponse(['error' => $e->getMessage()], $e->getCode())
            : new JsonResponse(['error' => 'Internal Server Error'], 500);
    }
}
