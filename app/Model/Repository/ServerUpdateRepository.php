<?php
namespace App\Model\Repository;

use App\Model\Server;
use Doctrine\ORM\EntityRepository;
use App\Model\ServerUpdate;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class ServerUpdateRepository extends EntityRepository
{
    /**
     * @return ServerUpdate[]
     */
    protected function findActive()
    {
        return $this->buildActiveQuery()->getQuery()->getResult();
    }

    /**
     * @return ServerUpdate[]
     */
    public function findSheduledForAutoRelease()
    {
        return $this->buildActiveQuery()
            ->andWhere('u.taken = :taken')
            ->andWhere('u.autoRelease IS NOT NULL')
            ->andWhere('u.autoRelease <= :datetime')
            ->setParameter('taken', true)
            ->setParameter('datetime', new \DateTime())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return ServerUpdate[]
     */
    public function findSheduledForReminder()
    {
        return $this->buildActiveQuery()
            ->andWhere('u.taken = :taken')
            ->andWhere('u.createdAt <= :datetime')
            ->andWhere('u.reminderSent = :reminderSent')
            ->setParameter('taken', true)
            ->setParameter('datetime', new \DateTime('-3 days'))
            ->setParameter('reminderSent', false)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return ServerUpdate[]
     */
    public function findSheduledForInactivityRelease()
    {
        return $this->buildActiveQuery()
            ->andWhere('u.taken = :taken')
            ->andWhere('u.createdAt <= :datetime')
            ->setParameter('taken', true)
            ->setParameter('datetime', new \DateTime('-7 days'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @return QueryBuilder
     */
    protected function buildActiveQuery()
    {
        return $this->createQueryBuilder('u')
            //->from(Server::class, 's')
            ->join(Server::class, 's', Join::WITH, '1=1')
            ->join('s.currentStatus', 'u2')//, Join::ON, 's.currentStatus IS NOT NULL')
            ->andWhere('u.id = u2.id')
            ->andWhere('s.currentStatus IS NOT NULL');
    }
}
