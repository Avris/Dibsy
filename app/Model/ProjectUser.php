<?php
namespace App\Model;

use Avris\Micrus\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use ICanBoogie\DateTime;

/**
 * @ORM\Entity
 **/
class ProjectUser implements \JsonSerializable
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $canManage;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="users")
     **/
    protected $project;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     **/
    protected $user;

    public function __construct(Project $project = null, User $user = null, $canManage = false, $active = true)
    {
        $this->project = $project;
        $this->user = $user;
        $this->canManage = $canManage;
        $this->active = $active;
        $this->createdAt = new DateTime();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getCanManage()
    {
        return $this->canManage;
    }

    /**
     * @param bool $canManage
     * @return $this
     */
    public function setCanManage($canManage)
    {
        $this->canManage = (bool) $canManage;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $active
     * @return ProjectUser
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @param DateTime $createdAt
     * @return ProjectUser
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @param Project $project
     * @return ProjectUser
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * @param User $user
     * @return ProjectUser
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'canManage' => $this->canManage,
            'createdAt' => $this->createdAt->format(\DateTime::ISO8601),
            'active' => $this->active,
        ];
    }
}
