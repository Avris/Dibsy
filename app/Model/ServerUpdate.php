<?php
namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use ICanBoogie\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\ServerUpdateRepository")
 **/
class ServerUpdate implements \JsonSerializable
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $branch;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $taken;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $autoRelease;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $reminderSent = false;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="serverUpdates")
     **/
    protected $user;

    /**
     * @var Server
     * @ORM\ManyToOne(targetEntity="Server", inversedBy="updates")
     **/
    protected $server;

    public function __construct(Server $server = null, User $user = null, $taken = true)
    {
        $this->server = $server;
        $this->user = $user;
        $this->taken = $taken;

        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param string $branch
     * @return ServerUpdate
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isTaken()
    {
        return $this->taken;
    }

    /**
     * @param boolean $taken
     * @return ServerUpdate
     */
    public function setTaken($taken)
    {
        $this->taken = $taken;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getAutoRelease()
    {
        return $this->autoRelease;
    }

    /**
     * @param DateTime $autoRelease
     * @return ServerUpdate
     */
    public function setAutoRelease($autoRelease)
    {
        $this->autoRelease = $autoRelease;
        return $this;
    }

    /**
     * @return $this
     */
    public function reminderSent()
    {
        $this->reminderSent = true;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ServerUpdate
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param Server $server
     * @return ServerUpdate
     */
    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'branch' => $this->branch,
            'taken' => $this->taken,
            'createdAt' => $this->createdAt->format(\DateTime::ISO8601),
            'user' => $this->user,
            'autoRelease' => $this->autoRelease ? $this->autoRelease->format(\DateTime::ISO8601) : null,
        ];
    }
}
