<?php
namespace App\Model;

use Avris\Micrus\Social\Model\BaseUser;
use Avris\Micrus\Tool\Security\RoleChecker;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User extends BaseUser implements \JsonSerializable
{
    const ROLE_MANAGER = 'ROLE_MANAGER';

    public static $availableRoles = [
        self::ROLE_USER => 'user',
        self::ROLE_MANAGER => 'manager',
        self::ROLE_ADMIN => 'admin',
    ];

    /**
     * @var ServerUpdate[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ServerUpdate", mappedBy="user", cascade={"persist"})
     **/
    protected $serverUpdates;

    /**
     * @var ProjectUser[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ProjectUser", mappedBy="user", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     **/
    protected $projects;

    public function __construct($email = null)
    {
        parent::__construct($email);
        $this->role = self::ROLE_MANAGER;
        $this->serverUpdates = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    public function getRoleName()
    {
        return static::$availableRoles[$this->role];
    }

    /**
     * @return ProjectUser[]|ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @return ArrayCollection|Project[]
     */
    public function getActiveProjects()
    {
        return $this->projects->matching(
            Criteria::create()->andWhere(Criteria::expr()->eq('active', true))
        )->map(function (ProjectUser $projectUser) {
            return $projectUser->getProject();
        });
    }

    public function canSeeProject(RoleChecker $roleChecker, Project $project)
    {
        if ($roleChecker->isUserGranted(self::ROLE_ADMIN, $this)) {
            return true;
        }

        return $this->getProjectUser($project) !== false;
    }

    public function cannotSeeProject(RoleChecker $roleChecker, Project $project)
    {
        return !$this->canSeeProject($roleChecker, $project);
    }

    public function canManageProject(RoleChecker $roleChecker, Project $project = null)
    {
        if ($roleChecker->isUserGranted(self::ROLE_ADMIN, $this)) {
            return true;
        }

        if ($project === null) {
            return $roleChecker->isUserGranted(self::ROLE_MANAGER, $this);
        }

        $projectUser = $this->getProjectUser($project);

        return $projectUser && $projectUser->getCanManage();
    }

    /**
     * @param Project $project
     * @return ProjectUser
     */
    public function getProjectUser(Project $project)
    {
        return $this->projects->matching(Criteria::create()
            ->andWhere(Criteria::expr()->eq('user', $this))
            ->andWhere(Criteria::expr()->eq('project', $project))
            ->andWhere(Criteria::expr()->eq('active', true))
        )->first();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'identifier' => $this->email,
            'active' => $this->active,
            'avatar' => $this->avatar,
        ];
    }
}
