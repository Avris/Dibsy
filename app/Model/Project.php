<?php
namespace App\Model;

use App\Service\Github\GithubService;
use Avris\Micrus\Bootstrap\Event;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\NotFoundException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use ICanBoogie\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\ProjectRepository")
 **/
class Project implements \JsonSerializable
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $urlTemplate;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $githubRepo;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var Server[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Server", mappedBy="project", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt"="ASC"})
     **/
    protected $servers;

    /**
     * @var ProjectUser[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ProjectUser", mappedBy="project", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"canManage"="DESC", "createdAt"="ASC"})
     **/
    protected $users;

    /**
     * @var ProjectUser[]
     */
    protected $removedUsers = [];

    /**
     * @var UploadedFile
     */
    protected $attachment;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $logo;

    /**
     * @var Authenticator[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Authenticator", mappedBy="project", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt"="ASC"})
     **/
    protected $authenticators;

    public function __construct($name)
    {
        $this->name = $name;
        $this->servers = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->authenticators = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrlTemplate()
    {
        return $this->urlTemplate;
    }

    /**
     * @param string $urlTemplate
     * @return Project
     */
    public function setUrlTemplate($urlTemplate)
    {
        $this->urlTemplate = $urlTemplate;
        return $this;
    }

    public function getUrl($server)
    {
        return $this->urlTemplate
            ? str_replace('{{server}}', $server, $this->urlTemplate)
            : null;
    }

    /**
     * @return Server[]|ArrayCollection
     */
    public function getServers()
    {
        return $this->servers;
    }

    public function addServers(Server $server)
    {
        $server->setProject($this);
        $this->servers->add($server);

        return $this;
    }

    public function removeServers(Server $server)
    {
        $this->servers->removeElement($server);

        return $this;
    }

    /**
     * @param $name
     * @return Server
     * @throws NotFoundException
     */
    public function getServer($name)
    {
        foreach ($this->servers as $server) {
            if ($server->getName() === $name) {
                return $server;
            }
        }

        throw new NotFoundException(sprintf('Server %s not found', $name));
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users->filter(function (ProjectUser $projectUser) {
            return $projectUser->isActive() && $projectUser->getUser()->isActive();
        })->map(function (ProjectUser $projectUser) {
            return $projectUser->getUser();
        });
    }

    public function getUserIds()
    {
        return $this->getUsers()->map(function (User $user) {
            return $user->getId();
        })->toArray();
    }

    public function getManagers()
    {
        return $this->users->filter(function (ProjectUser $projectUser) {
            return $projectUser->getCanManage() && $projectUser->isActive() && $projectUser->getUser()->isActive();
        })->map(function (ProjectUser $projectUser) {
            return $projectUser->getUser();
        });
    }

    /**
     * @return ProjectUser[]|ArrayCollection
     */
    public function getRequests()
    {
        return $this->users->filter(function (ProjectUser $projectUser) {
            return !$projectUser->isActive() && $projectUser->getUser()->isActive();
        })->map(function (ProjectUser $projectUser) {
            return $projectUser->getUser();
        });
    }

    /**
     * @return ProjectUser[]|ArrayCollection
     */
    public function getProjectUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return ProjectUser
     */
    public function getProjectUser(User $user)
    {
        return $this->users->matching(Criteria::create()->andWhere(Criteria::expr()->eq('user', $user)))->first();
    }

    /**
     * @param string $identifier
     * @return ProjectUser
     * @throws NotFoundException
     */
    public function getProjectUserByIdentifier($identifier)
    {
        $projectUser = $this->users->filter(function (ProjectUser $projectUser) use ($identifier) {
            return $projectUser->getUser()->getIdentifier() === $identifier;
        })->first();

        if (!$projectUser) {
            throw new NotFoundException(sprintf('User %s not found', $identifier));
        }

        return $projectUser;
    }

    public function addProjectUsers()
    {

    }

    public function removeProjectUsers(ProjectUser $projectUser)
    {
        $this->users->removeElement($projectUser);
        $this->removedUsers[] = $projectUser->getUser();

        return $this;
    }

    /**
     * @param User $manager
     * @return bool
     */
    public function hasManager(User $manager)
    {
        return $this->users->exists(function ($i, ProjectUser $projectUser) use ($manager) {
            return $projectUser->getCanManage() && $projectUser->getUser() == $manager;
        });
    }

    /**
     * @return ProjectUser[]
     */
    public function getRemovedUsers()
    {
        return $this->removedUsers;
    }

    public function addUser(User $user, $canManage = false, $active = true)
    {
        $this->users->add(new ProjectUser($this, $user, $canManage, $active));

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param UploadedFile $attachment
     */
    public function setAttachment(UploadedFile $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    public function handleFileUpload($dir)
    {
        if ($this->attachment) {
            $this->logo = $this->attachment->moveToRandom($dir, $this->logo);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getGithubRepo()
    {
        return $this->githubRepo;
    }

    /**
     * @param string $githubRepo
     * @return Project
     */
    public function setGithubRepo($githubRepo)
    {
        if (!$githubRepo) {
            $this->githubRepo = null;
            return $this;
        }

        if (!preg_match('#' . GithubService::REPO_REGEX . '#i', $githubRepo, $matches)) {
            $this->githubRepo = null;
            return $this;
        }

        $this->githubRepo = $matches[1];
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return Authenticator[]|ArrayCollection
     */
    public function getApiKeys()
    {
        return $this->authenticators->matching(Criteria::create()
            ->where(Criteria::expr()->eq('type', Authenticator::TYPE_API_KEY))
        );
    }

    /**
     * @param Authenticator $key
     * @return $this
     */
    public function addApiKeys(Authenticator $key)
    {
        $this->authenticators->add($key);
        $key->setType(Authenticator::TYPE_API_KEY);
        $key->setProject($this);
        $key->set('token', $this->generateToken());

        return $this;
    }

    /**
     * @param Authenticator $key
     * @return $this
     * @throws NotFoundException
     */
    public function removeApiKeys(Authenticator $key)
    {
        $this->authenticators->removeElement($key);
        $key->setProject(null);

        return $this;
    }

    /**
     * @return Authenticator[]|ArrayCollection
     */
    public function getWebhooks(Event $event = null)
    {
        return $this->authenticators->filter(function (Authenticator $webhook) use ($event) {
            return $webhook->getType() === Authenticator::TYPE_WEBHOOK
                && (!$event || in_array($event->getName(), $webhook->getPayload()['events']));
        });
    }

    /**
     * @param Authenticator $webhook
     * @return $this
     */
    public function addWebhooks(Authenticator $webhook)
    {
        $this->authenticators->add($webhook);
        $webhook->setType(Authenticator::TYPE_WEBHOOK);
        $webhook->setProject($this);
        $webhook->set('token', $this->generateToken());

        return $this;
    }

    /**
     * @param Authenticator $webhook
     * @return $this
     */
    public function removeWebhooks(Authenticator $webhook)
    {
        $this->authenticators->removeElement($webhook);
        $webhook->setProject(null);

        return $this;
    }

    /**
     * @return string
     */
    protected function generateToken()
    {
        return hash('sha256', mt_rand());
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'urlTemplate' => $this->urlTemplate,
            'githubRepo' => $this->githubRepo,
            'createdAt' => $this->createdAt->format(\DateTime::ISO8601),
            'servers' => $this->servers->toArray(),
            'users' => $this->users->toArray(),
            'logo' => $this->logo,
        ];
    }
}
