<?php
namespace App\Model;

use Avris\Micrus\Model\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use ICanBoogie\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\ServerRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="name_unique", columns={"project_id","name"})})
 **/
class Server implements \JsonSerializable
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $url;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="servers")
     **/
    protected $project;

    /**
     * @var ServerUpdate[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="ServerUpdate", mappedBy="server", cascade={"persist"})
     **/
    protected $updates;

    /**
     * @var ServerUpdate
     * @ORM\OneToOne(targetEntity="ServerUpdate")
     */
    protected $currentStatus;

    public function __construct(Project $project = null, $name = null, $url = null)
    {
        $this->project = $project;
        $this->name = $name;
        $this->url = $url;
        $this->createdAt = new DateTime();
        $this->updates = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Server
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getFullName()
    {
        return $this->project->getName() . '/' . $this->name;
    }

    public function __toString()
    {
        return $this->getFullName();
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Server
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlDisplay()
    {
        return $this->url ?: $this->project->getUrl($this->name);
    }

    /**
     * @return ServerUpdate[]|ArrayCollection
     */
    public function getUpdates()
    {
        return $this->updates;
    }

    /**
     * @param ServerUpdate $update
     * @return Server
     */
    public function addUpdates(ServerUpdate $update)
    {
        $this->updates->add($update);
        $update->setServer($this);
        $this->setCurrentStatus($update);

        return $this;
    }

    /**
     * @param ServerUpdate $updates
     * @return Server
     */
    public function removeUpdates(ServerUpdate $updates)
    {
        $this->updates->remove($updates);

        return $this;
    }

    /**
     * @return ServerUpdate
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus ?: new ServerUpdate($this, null, false);
    }

    /**
     * @param ServerUpdate $serverUpdate
     * @return $this
     */
    public function setCurrentStatus(ServerUpdate $serverUpdate)
    {
        $this->currentStatus = $serverUpdate;

        return $this;
    }

    /**
     * @param int $limit
     * @return ServerUpdate[]|ArrayCollection
     */
    public function getRecentHistory($limit = 25)
    {
        return $this->updates->matching(
            Criteria::create()
                ->orderBy(['createdAt' => Criteria::DESC])
                ->setMaxResults($limit)
        );
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Server
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;
        return $this;
    }

    public function take(ServerUpdate $update)
    {
        $update->setTaken(true);
        $this->addUpdates($update);

        return $this;
    }

    public function release(User $user = null)
    {
        $update = (new ServerUpdate())
            ->setUser($user)
            ->setTaken(false);
        $this->addUpdates($update);

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->getUrlDisplay(),
            'currentStatus' => $this->currentStatus,
        ];
    }
}
