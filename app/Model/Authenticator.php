<?php
namespace App\Model;

use Avris\Micrus\Social\Model\BaseAuthenticator;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Avris\Micrus\Social\Model\AuthenticatorRepository")
 */
class Authenticator extends BaseAuthenticator
{
    const TYPE_INVITATION = 'invitation';
    const TYPE_API_KEY = 'api_key';
    const TYPE_WEBHOOK = 'webhook';

    protected static $webhookEvents = [
        'updateServer',
        'updateProject',
    ];

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="authenticators")
     **/
    protected $project;

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Authenticator
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * @return string[]
     */
    public static function getWebhookEvents()
    {
        return self::$webhookEvents;
    }
}
