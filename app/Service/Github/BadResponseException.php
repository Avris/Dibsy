<?php
namespace App\Service\Github;

class BadResponseException extends \Exception
{
    public function __construct(\GuzzleHttp\Exception\BadResponseException $original)
    {
        parent::__construct($original->getMessage(), $original->getCode(), $original);
    }
}
