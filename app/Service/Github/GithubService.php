<?php
namespace App\Service\Github;

use App\Model\Project;
use Avris\Micrus\Model\User\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class GithubService
{
    const REPO_REGEX = '^(?:https?:\/\/github\.com\/)?([\w-]+\/[\w-]+)(?:\.git)?$';

    /** @var string */
    protected $token = false;

    /** @var Client */
    protected $httpClient;

    public function __construct(UserInterface $user, Client $httpClient)
    {
        foreach ($user->getAuthenticators('github') as $auth) {
            $this->token = $auth->getPayload()['accessToken'];
        }

        $this->httpClient = $httpClient;
    }

    public function fetchBranches(Project $project)
    {
        if (!$project->getGithubRepo()) {
            throw new NoRepoException;
        }

        if (!$this->token) {
            throw new NoTokenException;
        }

        try {
            $res = $this->httpClient->request(
                'GET',
                'https://api.github.com/repos/' . $project->getGithubRepo() . '/branches?per_page=1000',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->token,
                    ],
                ]
            );

            return array_map(function($branch) {
                return $branch['name'];
            }, json_decode($res->getBody(), true));
        } catch (ClientException $e) {
            throw new BadResponseException($e);
        }
    }
}
