<?php
namespace App\Service;

use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\MicrusJs\JsVarsInterface;

class JsVars implements JsVarsInterface
{
    /** @var string */
    protected $socketHost;

    /** @var RouteMatch */
    protected $routeMatch;

    public function __construct($socketHost = null)
    {
        $this->socketHost = $socketHost;
    }

    /** @return array */
    public function getJsVars()
    {
        return [
            'socketHost' => $this->socketHost,
        ];
    }
}
