<?php
namespace App\Service;

use App\Controller\ProjectController;
use App\Model\Authenticator;
use App\Model\Project;
use App\Model\ProjectUser;
use App\Model\User;
use Avris\Micrus\Controller\Http\CookieBag;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Routing\Service\Router;
use Avris\Micrus\Social\MailManagerInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Avris\Micrus\Bootstrap\EventDispatcherInterface;
use Doctrine\ORM\EntityManager;

class UserManager extends \Avris\Micrus\Social\UserManager
{
    /** @var InvitationService */
    protected $invitation;

    public function __construct(
        EntityManager $em,
        SecurityManager $sm,
        CryptInterface $crypt,
        RequestInterface $request,
        FlashBag $flashBag,
        MailManagerInterface $mailManager,
        Router $router,
        EventDispatcherInterface $dispatcher,
        InvitationService $invitation
    ) {
        parent::__construct($em, $sm, $crypt, $request, $flashBag, $mailManager, $router, $dispatcher);
        $this->invitation = $invitation;
    }

    public function login(User $user, CookieBag $responseCookies)
    {
        $redirect = parent::login($user, $responseCookies);
        $invitations = $this->request->getSession(ProjectController::SESSION_INVITATIONS);

        if ($redirect === 'home' && !empty($invitations)) { //success
            $auths = $this->em->getRepository(Authenticator::class)->findBy(['id' => $invitations]);

            /** @var Authenticator $auth */
            foreach ($auths as $auth) {
                /** @var Project $project */
                $project = $this->em->getRepository(Project::class)->find($auth->getPayload()['project']);

                if ($this->invitation->accept($user, $auth, $project)) {
                    $this->flashBag->add(
                        FlashBag::SUCCESS,
                        l('entity.Project.invite.granted', ['project' => $project->getName()])
                    );
                }
            }

            $this->request->getSession()->delete(ProjectController::SESSION_INVITATIONS);
        }

        return $redirect;
    }
}