<?php
namespace App\Service;

use App\Event\UpdateProjectEvent;
use App\Event\UpdateServerEvent;
use App\Model\Project;
use Avris\Micrus\Bootstrap\Event;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

class WebhookDispatcher
{
    /** @var Client */
    protected $httpClient;

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function onUpdateServer(UpdateServerEvent $event)
    {
        $this->dispatch(
            $event->getServer()->getProject(),
            $event,
            [
                'projectName' => $event->getServer()->getProject()->getName(),
                'server' => $event->getServer(),
            ]
        );
    }

    public function onUpdateProject(UpdateProjectEvent $event)
    {
        $this->dispatch(
            $event->getProject(),
            $event,
            [
                'project' => $event->getProject(),
            ]
        );
    }

    protected function dispatch(Project $project, Event $event, array $data)
    {
        $data = ['event' => $event->getName()] + $data;

        foreach ($project->getWebhooks($event) as $webhook) {
            try {
                $this->httpClient->request('POST', $webhook->get('url'), [
                    'json' => $data,
                    'headers' => ['X-Token' => $webhook->get('token')]
                ]);
            } catch (TransferException $e) {}
        }
    }
}
