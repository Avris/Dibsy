<?php
namespace App\Service;

use App\Model\Project;
use App\Model\Server;
use Avris\Micrus\Controller\Routing\Event\GenerateRouteEvent;
use Avris\Micrus\Model\AutomatcherSpecialTags;

class ServerMatcher implements AutomatcherSpecialTags
{
    public function getSpecialTags()
    {
        return [
            'server_name' => function (\ReflectionParameter $parameter, $tagValue, array $outTags) {
                return isset($outTags['name']) && $outTags['name'] instanceof Project
                    ? $outTags['name']->getServer($tagValue)
                    : null;
            }
        ];
    }

    public function onGenerateRoute(GenerateRouteEvent $event)
    {
        $params = $event->getParams();
        if (!isset($params['server']) || !$params['server'] instanceof Server) {
            return;
        }

        $params['name'] = $params['server']->getProject()->getName();
        $params['server_name'] = $params['server']->getName();
        unset($params['server']);

        $event->setParams($params);
    }
}
