<?php
namespace App\Service\Socket;

use App\Model\User;
use Avris\Bag\Bag;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Micrus\Tool\Config\ParametersProvider;
use Avris\Micrus\Tool\Security\Crypt;
use Avris\Micrus\Tool\Security\SecurityManager;
use Doctrine\ORM\EntityManager;
use Hoa\Event\Bucket;
use Hoa\Socket\Server;
//use Hoa\Websocket\Server as WebServer;
use Hoa\Stream\Context;
use Symfony\Component\Console\Output\OutputInterface;

class SocketServer implements ParametersProvider
{
    /** @var string */
    protected $host;

    /** @var WebServer */
    protected $server;

    /** @var EntityManager */
    protected $em;

    /** @var Crypt */
    protected $crypt;

    /** @var UserProviderInterface */
    protected $userProvider;

    /** @var string */
    protected $secret;

    /** @var string|null */
    protected $context = null;

    /** @var OutputInterface */
    protected $output;

    /**
     * @param Bag $config $host
     * @param EntityManager $em
     * @param UserProviderInterface $userProvider
     * @param Crypt $crypt
     * @param string $secret
     */
    public function __construct(
        Bag $config,
        EntityManager $em,
        UserProviderInterface $userProvider,
        Crypt $crypt,
        $secret = null
    ) {
        $this->host = $config->get('host');
        $this->em = $em;
        $this->crypt = $crypt;
        $this->userProvider = $userProvider;
        $this->secret = $secret;

        $ssl = $config->get('ssl', []);
        if (!empty($ssl)) {
            $context = Context::getInstance('Default');
            $context->setOptions(['ssl' => $ssl]);
            $this->context = 'Default';
        }
    }

    /**
     * @param OutputInterface|null $output
     * @return WebServer
     * @throws \Hoa\Event\Exception
     */
    public function configure(OutputInterface $output = null)
    {
        $this->output = $output;

        $this->server = new WebServer(new Server($this->host, 30, -1, $this->context));
        $this->server->getConnection()->setNodeName(SocketNode::class);
        $this->server->on('open', [$this, 'onOpen']);
        $this->server->on('message', [$this, 'onMessage']);
        $this->server->on('close', [$this, 'onClose']);

        $this->log('Configured host: ' . $this->host);

        return $this->server;
    }

    /**
     * @return WebServer
     */
    public function get()
    {
        return $this->server;
    }

    public function onOpen(Bucket $bucket)
    {
        $this->log('New connection');

        /** @var WebServer $connection */
        $connection = $bucket->getSource();

        list(, $encryptedCookie, $projectName) = explode('/', $connection->getRequest()->getUrl() . '/');

        if (!($user = $this->findUser(urldecode($encryptedCookie)))) {
            $this->log('Invalid authentication: ' . $encryptedCookie);
            $connection->close(WebServer::CLOSE_POLICY_ERROR, 'Invalid authentication');
            return;
        }

        /** @var SocketNode $node */
        $node = $connection->getConnection()->getCurrentNode();
        $node->setUserId($user->getId());
        $node->setProjectName($projectName ?: null);

        $this->log(sprintf('Authenticated as ' . $user->getIdentifier()));
    }

    /**
     * @param string $encryptedCookie
     * @return User|null
     */
    protected function findUser($encryptedCookie)
    {
        $decrypted = $this->crypt->decrypt($encryptedCookie);
        if ($decrypted === $this->secret) {
            return new User('system@dibsy');
        }

        list($identifier, $cookie) = explode('|',  $decrypted . '|');

        if (!$identifier || !$cookie) {
            return null;
        }

        $this->em->clear();
        $user = $this->userProvider->getUser($identifier);

        if (!$user) {
            return null;
        }

        foreach ($user->getAuthenticators(SecurityManager::AUTHENTICATOR_COOKIE) as $auth) {
            if ($auth->isValid() && $auth->getPayload() === $cookie) {
                return $user;
            }
        }

        return null;
    }

    public function onMessage(Bucket $bucket)
    {
        $this->log('> Message: ' . $bucket->getData()['message']);

        /** @var SocketNode $socketNode */
        $socketNode = $bucket->getSource()->getConnection()->getCurrentNode();
        $message = json_decode($bucket->getData()['message'], true);
        $sender = $socketNode->getUserId();
        $receivers = $message['to'];
        $projectName = isset($message['projectName']) ? $message['projectName'] : null;
        unset($message['to']);
        $sentTo = [];

        $bucket->getSource()->broadcastIf(
            function (SocketNode $node) use ($message, $sender, $receivers, $projectName, &$sentTo) {
                if ($node->interestedIn($sender, $receivers, $projectName)) {
                    $sentTo[] = $node->getUserId();
                    return true;
                }

                return false;
            },
            json_encode($message)
        );

        $this->log(count($sentTo)
            ? '< Sent to: ' . implode(', ', $sentTo)
            : '< No client interested'
        );
    }

    public function onClose(Bucket $bucket)
    {
        $this->log('Connection closed');
    }

    protected function log($messages)
    {
        if ($this->output) {
            $this->output->writeln($messages);
            return;
        }

        echo $messages . "\n";
    }

    public function getDefaultParameters()
    {
        return [
            'socket' => [
                'host' => 'ws://127.0.0.1:8889',
            ],
        ];
    }
}
