<?php
namespace App\Service\Socket;

use App\Model\User;
use Hoa\Websocket\Node;

class SocketNode extends Node
{
    /** @var int */
    protected $userId;

    /** @var string */
    protected $projectName;

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     * @return $this
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * @param int $sender
     * @param int[] $receivers
     * @param string|null $projectName
     * @return bool
     */
    public function interestedIn($sender, array $receivers, $projectName = null)
    {
        return $this->userId && $this->userId !== $sender && in_array($this->userId, $receivers)
            && ($projectName === null || $this->projectName === null || $projectName === $this->projectName);
    }
}
