<?php
namespace App\Service\Socket;

use App\Model\Authenticator;
use App\Model\User;
use App\Event\UpdateProjectEvent;
use App\Event\UpdateServerEvent;
use Avris\Bag\NotFoundException;
use Avris\Micrus\Controller\Http\HeaderBag;
use Avris\Micrus\Notify\Notify;
use Avris\Micrus\Tool\Security\Crypt;
use Avris\Micrus\Tool\Security\SecurityManager;
use Doctrine\ORM\EntityManager;
use Hoa\Socket\Client;
use Hoa\Websocket\Client as WebClient;

class SocketClient
{
    /** @var WebClient */
    protected $client;

    /** @var EntityManager */
    protected $em;

    /**
     * @param string $host
     * @param EntityManager $em
     * @param HeaderBag $headers
     * @param Crypt $crypt
     * @param Notify $notify
     * @param User $user
     * @param string $secret
     */
    public function __construct(
        $host,
        EntityManager $em,
        HeaderBag $headers,
        Crypt $crypt,
        Notify $notify,
        User $user = null,
        $secret = null
    ) {
        try {
            $this->client = new WebClient(new Client($host . '/' . urlencode($this->buildToken($crypt, $user, $secret))));
            $this->client->setHost($headers->has('host') ? (string) $headers->get('host') : 'localhost');
            $this->client->connect();
        } catch (\Exception $e) {
            $this->client = null;
            $notify->handle($e);
        }

        $this->em = $em;
    }

    private function buildToken(Crypt $crypt, User $user = null, $secret = null)
    {
        if (!$user) {
            return $crypt->encrypt($secret);
        }

        /** @var Authenticator $auth */
        $auth = $user->getAuthenticators(SecurityManager::AUTHENTICATOR_COOKIE)->first();

        if (!$auth) {
            throw new NotFoundException();
        }

        return $crypt->encrypt($user->getIdentifier() . '|' . $auth->getPayload());
    }

    public function onUpdateServer(UpdateServerEvent $event)
    {
        if (!$this->client) {
            return;
        }

        $server = $event->getServer();

        $this->client->send(json_encode([
            'event' => $event->getName(),
            'server' => $server,
            'projectName' => $server->getProject()->getName(),
            'to' => $server->getProject()->getUserIds(),
        ]));
    }

    public function onUpdateProject(UpdateProjectEvent $event)
    {
        if (!$this->client) {
            return;
        }

        $project = $event->getProject();

        $this->client->send(json_encode([
            'event' => $event->getName(),
            'project' => $project,
            'projectName' => $project->getName(),
            'to' => $project->getUserIds(),
        ]));
    }
}
