<?php
namespace App\Service;

use App\Model\Authenticator;
use App\Model\Project;
use App\Model\ProjectUser;
use App\Model\User;
use Avris\Bag\BagHelper;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Mailer\Mail\Address;
use Avris\Micrus\Social\MailManager;
use Avris\Micrus\Tool\Security\Crypt;
use Doctrine\ORM\EntityManager;

class InvitationService
{
    /** @var Crypt */
    protected $crypt;

    /** @var MailManager */
    protected $mailManager;

    /** @var EntityManager */
    protected $em;

    /** @var RouterInterface */
    protected $router;

    /**
     * @param Crypt $crypt
     * @param MailManager $mailManager
     * @param EntityManager $em
     * @param RouterInterface $router
     */
    public function __construct(
        Crypt $crypt,
        MailManager $mailManager,
        EntityManager $em,
        RouterInterface $router
    ) {
        $this->crypt = $crypt;
        $this->mailManager = $mailManager;
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * @param Project $project
     * @param User $inviter
     * @param string[] $invitees
     */
    public function invite(Project $project, User $inviter, $invitees)
    {
        foreach (BagHelper::toArray($invitees) as $invitee) {
            $token = $this->crypt->generateSecret();

            $auth = (new Authenticator())
                ->setType(Authenticator::TYPE_INVITATION)
                ->setPayload([
                    'project' => $project->getId(),
                    'token' => $token,
                    'email' => $invitee,
                    'inviter' => $inviter->getEmail(),
                ])
                ->setValidUntil(new \DateTime('+1 month'))
            ;

            $this->em->persist($auth);

            $this->mailManager->send('invitation', new Address($invitee), [
                'inviter' => $inviter,
                'project' => $project->getName(),
                'acceptLink' => $this->router->getUrl('projectAcceptInvitation', [
                    'name' => $project->getName(),
                    'token' => $token,
                ], true),
            ], true);
        }

        $this->em->flush();
    }

    /**
     * @param User $user
     * @param Authenticator $auth
     * @param Project|null $project
     * @return bool
     */
    public function accept(User $user, Authenticator $auth, Project $project = null)
    {
        if (!$project || $user->getProjectUser($project) !== false) {
            return false;
        }

        $invitationEmail = $auth->getPayload()['email'];

        $projectUser = new ProjectUser($project, $user, false, true);
        $auth->setUser($user);
        $auth->invalidate();

        $this->em->persist($projectUser);
        $this->em->persist($auth);
        $this->em->flush();

        $this->mailManager->send('invitationAccepted', new Address($auth->getPayload()['inviter']), [
            'invited' => $user->getEmail() === $invitationEmail
                ? $invitationEmail
                : l('mail.invitationAccepted.invitedAlias', [
                    'accountEmail' => $user->getEmail(),
                    'invitationEmail' => $invitationEmail
                ]),
            'project' => $project->getName(),
            'projectLink' => $this->router->getUrl('projectShow', [
                    'name' => $project->getName(),
                ], true),
        ], true);

        return true;
    }
}