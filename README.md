## Disby - Call dibs on test servers! ##

Dibsy helps team members to coordinate, which qa/staging server is currently being used by whom, and which ones are free to deploy to.

Using [Micrus framework](https://micrus.avris.it)

### Setup ###

	composer create-project avris/dibsy
	bin/micrus db:schema:create
	bin/micrus db:fixtures
    bin/micrus socket:server:run

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
