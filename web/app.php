<?php
require_once __DIR__ . '/../vendor/autoload.php';
$app = new \Avris\Micrus\Bootstrap\App(getenv('MICRUS_ENV') ?: 'prod', __DIR__ . '/..');
$request = \Avris\Micrus\Controller\Http\Request::fromGlobals();
$response = $app->handle($request);
$response->send();
$app->terminate();
