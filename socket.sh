#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ `ps aux | grep 'socket:server:run' | wc -l` -lt 2 ]
then
    php $DIR/bin/micrus socket:server:run --env=prod
fi
